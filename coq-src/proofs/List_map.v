Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import Encoding.List_map.
Require Import Coq.micromega.Lia.

Local Open Scope list.

Module Chunk.
  Module Valid.
    Definition t {a : Set} (chunk : list a):=
      (Lists.List.length chunk <= 12)%nat.
  End Valid.

  Module Saturated.
    Definition t {a : Set} (chunk : list a):=
      (Lists.List.length chunk >= 12)%nat.
  End Saturated.
End Chunk.

Fixpoint plain_unrolled_prefix_5_proof {A B : Set}
  (map_suffix : (A -> B) -> list A -> list B) (count : int) (f : A -> B)
  (l : list A)
  (H_map_suffix : forall l, map_suffix f l = Lists.List.map f l)
  {struct l} :
  plain_unrolled_prefix_5 map_suffix count f l =
    Lists.List.map f l.
Proof.
  do 5 (destruct l; [reflexivity|]).
  simpl.
  repeat f_equal.
  destruct CoqOfOCaml.Stdlib.le; [easy|].
  now rewrite plain_unrolled_prefix_5_proof.
Qed.

Fixpoint split_proof {A : Set} (chunks : list (list A)) (l : list A) :
  Lists.List.concat (Lists.List.map 
    (Lists.List.firstn 12) 
    (Lists.List.rev (List_map.AuxRecMap.split chunks l))) =
  Lists.List.concat (Lists.List.map 
    (Lists.List.firstn 12) (Lists.List.rev chunks)) ++ l.
Proof.
  refine (
    match l with
    | _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ => _
    | _ => _
    end
  ); with_strategy opaque [Lists.List.firstn] simpl;
    try now rewrite List.map_app, List.concat_app.
  rewrite split_proof.
  with_strategy opaque [Lists.List.firstn] simpl.
  rewrite List.map_app, List.concat_app.
  rewrite <- List.app_assoc.
  reflexivity.
Qed.

Module Split_helpers.
  Fixpoint split_size {A : Set}
  (chunks : list (list A)) (l : list A)
  (H_chunks : List.Forall Chunk.Saturated.t chunks) :
  match AuxRecMap.split chunks l with
  | [] => False
  | first :: rest => Chunk.Valid.t first /\ List.Forall Chunk.Saturated.t rest
  end.
  Proof.
    unfold Chunk.Valid.t.
    do 12 (destruct l;simpl; try (split; [lia | trivial])).
    apply split_size.
    constructor;[|trivial].
    unfold Chunk.Saturated.t; simpl.
    lia.
  Qed.

  (* @TODO https://https://gitlab.com/formal-land/json-data-encoding/-/issues/12 *)
  Axiom split__list12_rest :
    forall {A : Set} (l : list A) (chunk : list A) (chunks : list (list A)),
    AuxRecMap.split [] l = chunk::chunks ->
      Forall Chunk.Saturated.t chunks /\ Chunk.Valid.t chunk.
End Split_helpers.

Definition map_head_chunk_proof {A B : Set} 
  (chunk : list A) (function : A -> B) : 
  Chunk.Valid.t chunk -> AuxRecMap.map_head_chunk chunk function
  = Lists.List.map function chunk.
Proof.
  intro P.
  destruct chunk.
  { reflexivity. }
  { 
    do 12 try destruct chunk; trivial.
    unfold Chunk.Valid.t in P.
    simpl.
    simpl in P.
    specialize (length_is_pos chunk) as H_lp.
    lia.
  }
Qed.

Lemma map_tail_chunk_proof {A B : Set} (suffix : list A) (chunk : list B) 
(function : B -> A) : 
  Chunk.Saturated.t chunk -> AuxRecMap.map_tail_chunk suffix chunk function =
  firstn 12 (Lists.List.map function chunk) ++ suffix.
Proof.
  intro P.
  do 12 try destruct chunk;try (unfold Chunk.Saturated.t in P;
    unfold Datatypes.length in P;
    unfold length_aux in P;
    lia
  ).
  simpl.
  reflexivity.
Qed.

Fixpoint map_all_tail_chunks_proof {A B : Set} (suffix : list A) 
(chunks : list (list B)) (f_value : B -> A) {struct chunks}: 
Forall Chunk.Saturated.t chunks ->
  AuxRecMap.map_all_tail_chunks suffix chunks f_value =
    Lists.List.fold_left (fun suff chunk => 
    firstn 12 (Lists.List.map f_value chunk) ++ suff) chunks suffix.
Proof.
  intro P.
  destruct chunks.
  { reflexivity. }
  { Opaque firstn. 
    simpl.
    rewrite map_tail_chunk_proof.
    rewrite map_all_tail_chunks_proof.
    reflexivity.
    trivial.
    rewrite Forall_cons_iff in P; destruct P; trivial.
    rewrite Forall_cons_iff in P; destruct P; trivial.
  }
Qed.

Lemma firstnlength12_elim {A B: Set} (l : list A) (f : A -> B):
  (Lists.List.length l <= 12)%nat -> 
    firstn 12 (Lists.List.map f l) = Lists.List.map f l.
Proof.
  intros.
  do 13 (destruct l; [reflexivity|]; simpl).
  simpl in H.
  specialize (length_is_pos l) as H_pos.
  lia.
Qed.

Lemma chunked_tail_recursive_map_12_proof
  {A B : Set} (function : A -> B) (l : list A) :
  chunked_tail_recursive_map_12 function l = 
      Lists.List.fold_left
        (fun suff chunk => firstn 12 (Lists.List.map function chunk) ++ suff)
        (AuxRecMap.split [] l) [].
Proof.
  destruct l.
  { trivial. }
  unfold chunked_tail_recursive_map_12.
  destruct AuxRecMap.split eqn:?H_sp.
  { trivial. }
  specialize (Split_helpers.split__list12_rest _ _ _ H_sp) as satval.
  destruct satval as [H_sat H_val].
  rewrite map_head_chunk_proof.  
  rewrite map_all_tail_chunks_proof.
  {
    simpl.
    rewrite app_nil_r. 
    unfold Chunk.Valid.t in H_val.
    rewrite firstnlength12_elim.
    reflexivity.
    lia.
  }
  { simpl;trivial. }
  { trivial. }
Qed.

Fixpoint fold_right_concat_map {A B : Set} (f : A -> list B) (l : list A) :
  Lists.List.fold_right (fun chunk suffix => f chunk ++ suffix) [] l =
  Lists.List.concat (Lists.List.map f l).
Proof.
  destruct l as [|x l]; simpl; [reflexivity|].
  f_equal.
  apply fold_right_concat_map.
Qed.

Fixpoint concat_map_map {A B C : Set}
  (f : A -> list B) (g : B -> C) (l : list A) :
  Lists.List.concat (Lists.List.map (fun x => Lists.List.map g (f x)) l) =
  Lists.List.map g (Lists.List.concat (Lists.List.map f l)).
Proof.
  destruct l as [|x l]; simpl; [reflexivity|].
  rewrite Lists.List.map_app.
  f_equal.
  apply concat_map_map.
Qed.

Lemma chunked_tail_recursive_map_12_eq_map
  {A B : Set} (function : A -> B) (l : list A) :
  chunked_tail_recursive_map_12 function l =
  Lists.List.map function l.
Proof.
  rewrite chunked_tail_recursive_map_12_proof.
  rewrite <- Lists.List.fold_left_rev_right.
  rewrite fold_right_concat_map.
  rewrite Lists.List.map_ext_in with (g :=
    fun chunk => Lists.List.map function (firstn 12 chunk)
  ).
  { 
    rewrite concat_map_map.
    rewrite split_proof.
    simpl.
    reflexivity.
  }
  {
    intros.
    apply Lists.List.firstn_map.
  }
Qed.

Lemma faster_map_proof {A B : Set} (f : A -> B) (l : list A) :
  faster_map f l = Lists.List.map f l.
Proof.  
  unfold faster_map.
  rewrite plain_unrolled_prefix_5_proof; [reflexivity| intro].
  rewrite chunked_tail_recursive_map_12_eq_map.
  reflexivity.
Qed.

Lemma map_pure_proof {A B : Set} (f : A -> B) (l : list A) :
  map_pure f l = Lists.List.map f l.
Proof.
  unfold map_pure.
  rewrite faster_map_proof.
  reflexivity.
Qed.